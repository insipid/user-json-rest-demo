require File.expand_path '../test_helper.rb', __FILE__

include Rack::Test::Methods

# Sort-of spec-like acceptance tests for the
# UserRESTJSONDemoApplication

# The success of the tests depends on the existence of the 
# fixures in test_helper.rb, to establish the data that these 
# tests are run against.

describe 'a get request' do
  it 'finds the existing alex user' do
    get '/users/alex'
    assert last_response.ok?
    assert_json last_response

    json = JSON.parse(last_response.body)
    assert_equal 'alex', json['username']
    assert_equal 'alex@example.org', json['email']
    # [NOTE:] We don't return the password but, if we did, 
    #   we would test it here.
    # assert_equal HASH('alexs3kret'), json['password']
  end

  it 'finds the existing bobby user' do
    u = user('bobby')
    get "/users/#{u[:username]}"
    assert last_response.ok?, "Failed getting #{u[:username]} (#{last_response.status}): #{last_response.body}"
    assert_json last_response
  end

  it 'does not find the nonexistent viv' do
    get '/users/viv'
    assert last_response.not_found?
  end
end

describe 'a bogus create request' do
  before do
    @u = user('foo')
  end

  it 'returns error on malformed JSON' do
    post '/users', 'this is not json'
    assert_equal 400, last_response.status
    assert_json_error 'could not parse', last_response
  end

  it 'requires a username in submitted json' do
    post '/users', @u.without(:username).to_json
    assert_equal 400, last_response.status
    assert_json_error 'missing username', last_response

    post '/users', @u.merge({:username => ''}).to_json
    assert_equal 400, last_response.status
    assert_json_error 'missing username', last_response
  end

  it 'requires an email in submitted json' do
    post '/users', @u.without(:email).to_json
    assert_equal 400, last_response.status
    assert_json_error 'missing email', last_response

    post '/users', @u.merge({:email => ''}).to_json
    assert_equal 400, last_response.status
    assert_json_error 'missing email', last_response
  end

  it 'requires a password in submitted json' do
    post '/users', @u.without(:password).to_json
    assert_equal 400, last_response.status
    assert_json_error 'missing password', last_response

    post '/users', @u.merge({:password => ''}).to_json
    assert_equal 400, last_response.status
    assert_json_error 'missing password', last_response
  end

  it 'returns an error on an invalid username' do
    post '/users', @u.merge({:username => 'user name'}).to_json
    assert_equal 400, last_response.status
    assert_json_error 'invalid username', last_response

    username = 'x' * 256
    post '/users', @u.merge({:username => username}).to_json
    assert_equal 400, last_response.status
    assert_json_error 'invalid username', last_response
  end
end

describe 'a successful create request' do
  it 'returns success with valid data' do
    u = user('danny')
    post '/users', u.to_json
    assert last_response.ok?, "Failed posting #{u[:username]} (#{last_response.status}): #{last_response.body}"
    assert_json last_response

    json = JSON.parse(last_response.body)
    assert_equal u[:username], json['username']
    assert_equal u[:email], json['email']
  end

  it 'can be fetched back' do
    u = user('eddy')
    post '/users', u.to_json
    assert last_response.ok?, "Failed posting #{u[:username]} (#{last_response.status}): #{last_response.body}"

    get "/users/#{u[:username]}"
    assert last_response.ok?, "Failed getting #{u[:username]} (#{last_response.status}): #{last_response.body}"
    assert_json last_response

    json = JSON.parse(last_response.body)
    assert_equal u[:username], json['username']
    assert_equal u[:email], json['email']
  end

  it 'will not duplicate usernames' do
    post '/users', user('bobby').merge({:email => 'bobby2@example.org'}).to_json
    assert_equal 400, last_response.status
    assert_json_error 'username already', last_response
  end

  it 'will not duplicate email addresses' do
    post '/users', user('bobby2').merge({:email => 'bobby@example.org'}).to_json
    assert_equal 400, last_response.status
    assert_json_error 'address already', last_response
  end
  
  it 'can register a really long username' do
    u = user('x' * 255)
    post '/users', u.merge({:email => 'xxxxx@example.org'}).to_json
    assert last_response.ok?, "Failed posting #{u[:username]} (#{last_response.status}): #{last_response.body}"
  end
  
  # [TODO:] What about really long email address?
end

describe 'a delete request' do
  it 'is unauthorized without an apikey' do
    delete '/users/alex'
    assert_equal 401, last_response.status
    assert_json_error 'not authorized', last_response
  end

  it 'is unauthorized with incorrect apikey' do
    delete '/users/alex?apikey=bazquux'
    assert_equal 401, last_response.status
    assert_json_error 'not authorized', last_response
  end

  it 'returns 404 for nonexistent user' do
    delete '/users/viv'
    assert last_response.not_found?
  end

  it 'successfully deletes with query string apikey' do
    get '/users/charlie'
    assert last_response.ok?
    assert_json last_response

    delete '/users/charlie?apikey=foobar'
    assert last_response.ok?
    
    get '/users/charlie'
    assert last_response.not_found?
  end

  it 'successfully deletes with apikey in json' do
    u = user('frankie')
    post '/users', u.to_json
    assert last_response.ok?, "Failed posting #{u[:username]} (#{last_response.status}): #{last_response.body}"

    delete "/users/#{u[:username]}", {:apikey => 'foobar'}.to_json
    assert last_response.ok?
    
    get "/users/#{u[:username]}"
    assert last_response.not_found?
  end
end

describe 'a validation request' do
  before do
    @u = user('alex')
    @v = user('bobby')
  end

  it 'returns 404 on nonexistent user' do
    post '/validate/viv', {:password => 'vivs3kret'}.to_json
    assert last_response.not_found?
  end

  it 'validates a correct password' do
    post "/validate/#{@u[:username]}", {:password => @u[:password]}.to_json
    assert last_response.ok?, "Failed validating #{@u[:username]} (#{last_response.status}): #{last_response.body}"
  end

  it 'validates a correct password' do
    post "/validate/#{@v[:username]}", {:password => @v[:password]}.to_json
    assert last_response.ok?, "Failed validating #{@v[:username]} (#{last_response.status}): #{last_response.body}"
  end

  it 'refutes an incorrect password' do
    post "/validate/#{@u[:username]}", {:password => 'xyzzy'}.to_json
    assert_equal 403, last_response.status
    assert_json_error 'invalid authentication', last_response
  end

  it 'refutes an incorrect password' do
    post "/validate/#{@v[:username]}", {:password => 'xyzzy'}.to_json
    assert_equal 403, last_response.status
    assert_json_error 'invalid authentication', last_response
  end
end    
