# Setup testing environment/libs
ENV['RACK_ENV'] = 'test'
require 'minitest/autorun'
require 'rack/test'
require "minitest/reporters"

# Test database file
TEST_DATABASE_FILE_PATH = "#{Dir.pwd}/db/test_users.db"

# Fixtures, for the tests
INITIAL_DATABASE_DATA = %w[ 
  alex    alex@example.org    alexs3krit 
  bobby   bobby@example.org   bobbys3krit
  charlie charlie@example.org charlies3krit
]

# Include actual application
require File.expand_path '../../application.rb', __FILE__

# Let's hide some of the noise from our backtraces
custom_backtrace_filter = Minitest::ExtensibleBacktraceFilter.new
custom_backtrace_filter.add_filter(/sinatra-1.4.5/)
custom_backtrace_filter.add_filter(/minitest-/)
custom_backtrace_filter.add_filter(/rack-/)

# Load the reporters with our custom backtrace filter
Minitest::Reporters.use!(Minitest::Reporters::DefaultReporter.new,
                         ENV,
                         custom_backtrace_filter)

# Delete/reset the test database
File.delete(TEST_DATABASE_FILE_PATH) if File.exist?(TEST_DATABASE_FILE_PATH)

# The tests expect an `app` method to return the application, but we
# want to tweak ours slightly.
def app
  # We make sure to use a test database, which we can prepopulate with
  # fixtures:
  UserRESTJSONDemoApplication.set :database_path, TEST_DATABASE_FILE_PATH
  UserRESTJSONDemoApplication.set :initial_database_data, INITIAL_DATABASE_DATA
end

# Monkey-patch hash class to add a way to return hash without some k/v
# pairs (a convenience method only):
class Hash
  def without(*args)
    self.select {|k, | !args.include? k }
  end
end

# Define a few asserts of our own; the first ensures the passed
# Rack::Response has a JSON content-type; the latter allows a
# RegExp-as-string to match a JSON-ified error message in the
# response.
#
def assert_json(response, msg = nil)
  json_mime_type = 'application/json'
  msg = message(msg) {"Expected '#{json_mime_type}', got #{response.headers['Content-Type']}"}
  assert_equal json_mime_type, response.headers['Content-Type'], msg
end

def assert_json_error(error, response, msg = nil)
  msg = message(msg) {"Expected /#{error}/i to match #{JSON.parse(response.body)['error']}"}
  assert_match Regexp.new(error, Regexp::IGNORECASE), JSON.parse(response.body)['error'], msg
end

# A convenience function for creating a dummy user hash out of a
# username; used in the acceptance tests.
def user(username)
  {:username => username,
    :email => "#{username}@example.org",
    :password => "#{username}s3krit"}
end
