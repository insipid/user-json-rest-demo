require "rubygems"
require "bundler/setup"

Bundler.require(:default)

require "./application.rb"

set :environment, ENV["RACK_ENV"].to_sym

# Configure path to database file
UserRESTJSONDemoApplication.set :database_path, "#{Dir.pwd}/db/users.db"

# Can configure preloaded users here:
# UserRESTJSONDemoApplication.set :initial_database_data, ['alex', 'alex@example.org', 'alexs3kret']

run UserRESTJSONDemoApplication
