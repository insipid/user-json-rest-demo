# user-json-rest-demo

Demonstration JSON/REST app for managing users, as output of "technical
task".

## Contents
[TOC]

## Specification

>    Design and build a JSON REST API which implements creation, reading
>    and deletion of users. There's no need to provide functionality to
>    update a user or to perform the creation, reading and deletion for
>    more than one user at a time. This should be written in a high-level
>    language of your choice (Python, Ruby, PHP, etc.) and can make use of
>    any libraries or framework (Flask, Sinatra, Django, etc.) of your
>    choice. For deletion of a user, an API key `foobar` should be provided
>    in the request as you see fit (i.e. as a header field, query string
>    parameter, etc.). There's no need to implement an API key model,
>    simply checking against a constant would be fine.
>
>    As part of the task, you should implement a `User` model which has
>    `username`, `email` and `password` fields. The `User` model should be
>    persistable, but there's no expectation that the storage engine would
>    be used in production, so a flat file database like SQLite would be
>    ideal. The password should be stored securely, using a hashing scheme
>    of your choice.

## Implementation

The `user-json-rest-demo` application has been created using
[ruby](https://www.ruby-lang.org/en/), with
[sinatra](http://www.sinatrarb.com/) as the web-framework, and
[data-mapper](http://datamapper.org/) and
[sqlite](http://www.sqlite.org/) for persistence.

For simplicity's sake, the sinatra application is all in a single
file: `application.rb`, which contains the `Sinatra::Base` subclass, as
well as the data-modelling, and relevant helper functions. (The next
step in a real application would be to split these up into separate
concerns; but as above, they're in one place for ease-of-use right now.)

There are acceptance tests in `test/test_application.rb` (which use
helpers in `test/test_helper.rb`), and these should cover the
behaviour as specified above, as well as as many "edge cases" as could
be speculated.

(The other files are deployment/configuration files, `Gemfile` for
`bundler`, `Guardfile` for autorunning the tests with
[guard](https://github.com/guard/guard), and `config.ru` for running
the server with [rack](http://rack.github.io/))

### Design/implementation notes

The application provides an HTTP REST API, which accepts and returns
JSON data. The API supports creating new users, reading user data, and
deleting users (with a correct API key), in an expected fashion.

It also allows validation of a password for a chosen user. [This was
outside the explicit specification, but we use it to test
password-validation functionality. Could be made "only in development
environment", etc., if necessary.]

The data is modelled by a `User` `DataMapper::Resource` model, which
contains `username`, `email`, and `password` fields (all of type
`String`); but the `password` attribute persists a bcrypt
salt-and-hash of the password, rather than the plaintext password
itself.

[bcrypt](https://en.wikipedia.org/wiki/Bcrypt) is used to securely
store the password (hashes), and was chosen as key-derivation
functions (like bcrypt, scrypt, PBKDF2) are (AFAIK) best-practice for
storing sensitive data like passwords; especially because the
derivation can have its "computational cost" increased as necessary,
to make validation as difficult as bearable for the service, to
maximise costs of trying to break the hashes. (See `BCRYPT_COST` in
`application.rb`)

It uses the [`bcrypt`](https://github.com/codahale/bcrypt-ruby) gem so
we don't have to write crypto code ourselves (which would be bad!)
Also, we don't even need to generate secure salts, since the library
does that for us. It is hooked into the `User` data model via
setter/getter methods, and the `BCrypt::Password` performs validation
using the `==` operator.

As mentioned in the specification, the API key is only tested against
a string (`"foobar"`) in this case. And usernames are validated
against a regular expression like this:
`/^[A-Za-z0-9_-]{1,255}$/`. These settings can be tweaked by the
constants `DEFAULT_API_KEY` and `USERNAME_VALIDATOR_REGEX` in
`application.rb` -- as can the actual validation functions, included
in the helper methods there.

Quirk: The `UserRESTJSONDemoApplication` class (the `Sinatra::Base`
subclass) can have settings applied to it, to specify (a) where the
sqlite database file should be, and (b) any data to initialise the
database with, like this:

    UserRESTJSONDemoApplication.set :database_path, 'test_users.db'
    UserRESTJSONDemoApplication.set :initial_database_data, ['foo', 'foo@example.org', 'foos3krit']

The `database_path` setting is just a file-path, and the
`initial_database_data` setting is an array of triplets of the form
`[username1, email1, password1, username2, email2, password2, ...]`

This has been done to allow the acceptance tests to be run against a
known database state, by using a "new" database file, with known data
inserted into it. (Which is necessary to be able to (e.g.) keep tests
independent and as decoupled as possible.) These "fixtures" are also
in `test/test_helper.rb`.

### API

#### `GET /users/joebloggs` - get user detail for 'joebloggs'
* Input: None
* Returns HTTP error 404 if user 'joebloggs' does not exist
* Returns JSON like `{'username': 'joebloggs', 'email': 'joebloggs@example.org'}` if successful
* NB. Only returns `username`, and `email`; not `password`.


#### `POST /users` - create a new user
* Input: JSON body like `{'username': 'joebloggs', 'email': 'joebloggs@example.org', 'password': 'joebloggss3krit'}`
* Returns HTTP 200 on success
* Returns HTTP error 400, with JSON content like `{'error': 'this is an error message'}` if there is an validation error (which can be any of: missing field in input JSON, duplicate username or email address, or an invalid username)
* Returns HTTP error 503, for any unknown error saving the user


#### `DELETE /users/joebloggs` - deletes the user 'joebloggs', with valid API key
* Input: JSON body like `{'apikey': 'foobar'}`
* Input: or as an HTTP parameter, like `DELETE /users/joebloggs?apikey=foobar`
* Returns HTTP 200 on success
* Returns HTTP error 401, if no valid API key is found
* Returns HTTP error 404, if user 'joebloggs' doesn't exist


#### `POST /validate/joebloggs` - validates a password for user 'joebloggs'
* Input: JSON body like `{'password': 'joebloggss3krit'}`
* Returns HTTP 200 on success
* Returns HTTP error 403, if password cannot be validated

## Deployment and execution

### Installation

`user-json-rest-demo` has been tested successfully using `ruby1.9.3`
on a "fresh" [DigitalOcean](https://www.digitalocean.com/) Ubuntu
14.04 LTS (x64) image.

From a clean instance, would need at least ruby (1.9.3), and git:

    sudo apt-get update
    sudo apt-get install ruby1.9.3 git

Also, need [`bundler`](http://bundler.io/) installed for fetching the
gems needed:

    sudo gem install bundler

Then, checkout the `user-json-rest-demo` git repo from BitBucket:

    git clone https://insipid@bitbucket.org/insipid/user-json-rest-demo.git

Use `bundler` to install the gems; but some may require more packages
to be installed. Again, from a "fresh" Ubuntu 14.04, at least the
build tools and the sqlite packages are necessary:

    sudo apt-get install build-essential libsqlite3-dev sqlite3

Install gems into vendor:

    cd user-json-rest-demo/
    bundle install --path=./vendor/bundle

Assuming that's successful, can run the acceptance tests like this:

    bundle exec ruby test/test_application.rb

Should look something like:

    # Running tests with run options --seed 50685:

    .......................

    Finished tests in 6.008516s, 3.8279 tests/s, 12.1494 assertions/s.

    23 tests, 73 assertions, 0 failures, 0 errors, 0 skips

To run the tests continually, using `guard`:

    bundle exec guard --notify=false

(I was having problems with the notifications, so disable them when I run guard.)

### Running the server

Fire up as you would any other rack server; e.g.:

    bundle exec rackup config.ru --host 127.0.0.1 --port 5678

With curl installed, we can now interact with the running service.

For example:

    $ curl localhost:5678/users/joebloggs -i && echo
    HTTP/1.1 404 Not Found
    [...]
    {"error":"No such user: joebloggs"}

    $ curl localhost:5678/users -i -X POST -H "Content-Type: application/json" --data-binary '{"username":"joebloggs", "email":"joebloggs@example.org"}' && echo
    HTTP/1.1 400 Bad Request
    [...]
    {"error":"Missing password"}

    $ curl localhost:5678/users -i -X POST -H "Content-Type: application/json" --data-binary '{"username":"joebloggs", "email":"joebloggs@example.org", "password":"joebloggss3krit"}' && echo
    HTTP/1.1 200 OK
    [...]
    {"username":"joebloggs","email":"joebloggs@example.org"}

    $ curl localhost:5678/users -i -X POST -H "Content-Type: application/json" --data-binary '{"username":"joebloggs", "email":"joebloggs@example.org", "password":"joebloggss3krit"}' && echo
    HTTP/1.1 400 Bad Request
    [...]
    {"error":"Username already exists"}

    $ curl localhost:5678/users/joebloggs -i && echo
    HTTP/1.1 200 OK
    [...]
    {"username":"joebloggs","email":"joebloggs@example.org"}

    $ curl localhost:5678/validate/joebloggs -i -X POST -H "Content-Type: application/json" --data-binary '{"password":"joebloggss3krit"}' && echo
    HTTP/1.1 200 OK
    [...]


    $ curl localhost:5678/validate/joebloggs -i -X POST -H "Content-Type: application/json" --data-binary '{"password":"notsos3krit"}' && echo
    HTTP/1.1 403 Forbidden
    [...]
    {"error":"Invalid authentication"}

    $ curl localhost:5678/users/joebloggs -i -X DELETE -H "Content-Type: application/json" --data-binary '{"apikey":"quux"}' && echo
    HTTP/1.1 401 Unauthorized
    [...]
    {"error":"Not authorized to delete; need valid apikey."}

    $ curl localhost:5678/users/joebloggs?apikey=quux -i -X DELETE && echo
    HTTP/1.1 401 Unauthorized
    [...]
    {"error":"Not authorized to delete; need valid apikey."}

    $ curl localhost:5678/users/joebloggs -i -X DELETE -H "Content-Type: application/json" --data-binary '{"apikey":"foobar"}' && echo
    HTTP/1.1 200 OK
    [...]
    

    $ curl localhost:5678/users/joebloggs -i && echo
    HTTP/1.1 404 Not Found
    [...]
    {"error":"No such user: joebloggs"}

    $ curl localhost:5678/users -i -X POST -H "Content-Type: application/json" --data-binary '{"username":"joebloggs", "email":"joebloggs@example.org", "password":"joebloggss3krit"}' && echo
    $ curl localhost:5678/users/joebloggs?apikey=foobar -i -X DELETE && echo
    HTTP/1.1 200 OK
    [...]