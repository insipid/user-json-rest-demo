require 'sinatra/base'
require 'sinatra/reloader'
require 'data_mapper'
require 'json'
require 'bcrypt'

DEFAULT_API_KEY = "foobar"
USERNAME_VALIDATOR_REGEX = /^[A-Za-z0-9_-]{1,255}$/
# The BCrypt library default value; tweak to change cost/security:
BCRYPT_COST = 10

class UserRESTJSONDemoApplication < Sinatra::Base
  # Reloader for development mode
  configure :development do
    register Sinatra::Reloader
  end

  # ------------------------------------------------------------------------
  # ----------------------- Models and data binding ------------------------
  # ------------------------------------------------------------------------
  class User
    include DataMapper::Resource
    property :id, Serial
    property :username, String, :length => 255
    property :email, String, :length => 255
    property :password, String, :length => 255

    # Expose the password property via accessors to store and return
    # (and compare) the bcrypt-hash-ified versions of the password.
    def password
      password_hash = super
      BCrypt::Password.new(password_hash)
    end

    def password=(password)
      super BCrypt::Password.create(password, :cost => BCRYPT_COST)
    end
  end

  # Fail early, and bubble-up
  User.raise_on_save_failure = true

  # ------------------------------------------------------------------------
  # ----------------------- Constructor and helpers ------------------------
  # ------------------------------------------------------------------------

  # We use the constructor to setup our data mapping, and to tweak
  # databse file settings.
  def initialize
    super
    if settings.respond_to? :database_path then
      database_filepath = settings.database_path
    else
      database_filepath = "#{Dir.pwd}/db/users.db"
    end

    DataMapper::setup(:default, "sqlite3://#{database_filepath}")

    DataMapper.finalize
    User.auto_upgrade!

    load_initial_data
  end

  # Helper function to "drop all" from database, and import initial data
  def load_initial_data
    if settings.respond_to? :initial_database_data then
      User.all.destroy
      settings.initial_database_data.each_slice(3) do |username, email, password|
        User.create(:username => username, :email => email, :password => password)
      end
    end
  end

  # These are slightly stubby methods; but should be clear where
  # to hook into if we want to change username validation, or how
  # authorization works, or what a valid API key, etc.
  #
  def is_valid_username?(username)
    USERNAME_VALIDATOR_REGEX =~ username
  end
  #
  def is_valid_api_key?(key)
    DEFAULT_API_KEY == key
  end

  def is_authorized?(request)
    # Load the apikey parameter from the JSON, if submitted;
    # otherwise, look for a standard HTTP parameter
    begin
      payload = JSON.parse(request.body.read)
      apikey = payload['apikey']
    rescue JSON::ParserError
      apikey = params.fetch('apikey', '')
    end
    is_valid_api_key? apikey
  end

  # Try to parse potential JSON; return error if fail
  def try_parse_json(request)
    begin
      payload = JSON.parse(request.body.read)
    rescue JSON::ParserError
      error 'Could not parse JSON'
    end
  end

  # Return HTTP status code, and a JSONified error message
  def error_code(code, message)
    halt code, {'Content-Type' => 'application/json'}, {:error => message}.to_json
  end

  # Shortcut for generic 400 error
  def error(message)
    error_code 400, message
  end

  # ------------------------------------------------------------------------
  # ----------------------- Routes -----------------------------------------
  # ------------------------------------------------------------------------

  get '/users/:username' do
    user = User.first(:username => params['username'])

    if user.nil? then
      error_code 404, "No such user: #{params['username']}"
    end

    content_type :json
    # Only return attributes we're explicit about
    { :username => user.attributes[:username], :email => user.attributes[:email]}.to_json
  end

  post '/users' do
    payload = try_parse_json request

    # Check for missing fields
    %w[username email password].each do |field|
      if payload.fetch(field, '').strip.empty? then
        error "Missing #{field}"
      end
    end

    error 'Invalid username' if !is_valid_username?(payload['username'])

    # Use DM to search for existing users with this username/email
    if !User.first(:username => payload['username']).nil? then
      error 'Username already exists'
    elsif !User.first(:email => payload['email']).nil? then
      error 'Email address already in use'
    end

    # Actually create user, and implicitly try to save it
    user = User.create(:username => payload['username'], :email => payload['email'], :password => payload['password'])

    # [TODO:] Could bubble-up data-mapper errors back to the end-user;
    # but right now we just say there was a problem saving
    error_code 503, 'Internal error saving user' unless user.saved?

    # Return JSONified hash
    content_type :json
    { :username => user.attributes[:username], :email => user.attributes[:email]}.to_json
  end

  delete '/users/:username' do
    user = User.first(:username => params['username'])
    error_code 404, "No such user: #{params['username']}" unless user
    error_code 401, 'Not authorized to delete; need valid apikey.' unless is_authorized? request
    user.destroy
  end

  post '/validate/:username' do
    user = User.first(:username => params['username'])
    error_code 404, "No such user: #{params['username']}" unless user

    payload = try_parse_json request
    
    if user.password == payload['password']
      status 200
      halt
    else
      error_code 403, 'Invalid authentication'
    end
  end
end
